/*
 * Segment.h
 *
 * Created: 18.04.2016 13:59:28
 *  Author: Jakob
 */ 


#ifndef SEGMENT_H_
#define SEGMENT_H_

/*
 * Function: Segment_init
 *		Initializes the driver for the 8 digit 7-Segment Display.
 *      The driver stores a value for each segment and automatically
 *      switches periodically (using Timer 1) throught the segments and displays the 
 *      value for each segment.
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.5.2016
 */
void Segment_init();

/*
 * Function: Segment_setValueForSegment
 *		Store the value of one segment in a array and display it automatically.
 *
 * Parameter: 
 *      in  segmentNum  The number of the segment that should be lighted (0-7)
 *      in  number      Number that should be set on the segment (0-9)
 *
 * preconditions: 
 *      the driver for the 7-Segment Display must be initialized with segment_init()
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.5.2016
 */
void Segment_setValueForSegment(uint8_t segmentNum, uint8_t number);

/*
 * Function: Segment_setPointOn
 *		Set the point of the selected segment on in the array
 *
 * Parameter: 
 *      in  segmentNum  The number of the segment, of which the point should be lighted (0-7)
 *
 * preconditions: 
 *      the driver for the 7-Segment Display must be initialized with segment_init()
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.5.2016
 */
void Segment_setPointOn(uint8_t segmentNum);

/*
 * Function: Segment_setPointOff
 *		Reset the point of the selected segment on in the array
 *
 * Parameter: 
 *      in  segmentNum  The number of the segment, of which the point should be turned off (0-7)
 *
 * preconditions: 
 *      the driver for the 7-Segment Display must be initialized with segment_init()
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.5.2016
 */
void Segment_setPointOff(uint8_t segmentNum);

/*
 * Function: Segment_setSign
 *		Display a minus sign (-) on the selected segment.
 *
 * Parameter: 
 *      in  segmentNum  The number of the segment, which should be set to a minus sign (0-7)
 *
 * preconditions: 
 *      the driver for the 7-Segment Display must be initialized with segment_init()
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.5.2016
 */
void Segment_setSign(uint8_t segmentNum);


#endif /* SEGMENT_H_ */