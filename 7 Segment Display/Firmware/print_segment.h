/*
 * print_segment.h
 *
 * Created: 19.04.2016 17:50:28
 *  Author: Jakob
 */ 


#ifndef PRINT_SEGMENT_H_
#define PRINT_SEGMENT_H_

#include "Segment.h"

/*
 * Function: print_uint
 *		Prints a uint32_t number on the 8 digit 7-Segment Display.
 *
 * Parameter: 
 *      in  printValue  uint value that should be displayed on the Display
 *
 * preconditions: 
 *      the driver for the 7-Segment Display must be initialized with segment_init()
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.5.2016
 */
void print_uint (uint32_t printValue);

/*
 * Function: print_uint
 *		Prints a int32_t number on the 8 digit 7-Segment Display.
 *
 * Parameter: 
 *      in  printValue  int value that should be displayed on the Display
 *
 * preconditions: 
 *      the driver for the 7-Segment Display must be initialized with segment_init()
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.5.2016
 */
void print_int(int32_t printValue);

#endif /* PRINT_SEGMENT_H_ */