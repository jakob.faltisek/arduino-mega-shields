/*
 * Segment.c
 *
 * Created: 18.04.2016 13:59:02
 *  Author: Jakob
 */ 

#include <avr/io.h>
#include <stdlib.h>
#define F_CPU					16000000
#include <util/delay.h>
#include <avr/interrupt.h>
#include "Segment.h"

#define		SEGMENT_SELECT		PORTC
#define		SEGMENT_VALUE		PORTA

#define		SEGMENT_OFF			10
#define		DISPLAY_SIGN		0xBF
#define		POINT				7

#define		DELAY_SHOW			50
#define		DELAY_SWITCH		10

#define		TIME_REFRESH		0.002

void Segment_refreshValues();
void Segment_lightNextSegment();

//the values 0-9 is for the number to display on the segments
//10 is to turn off the Segment
const uint8_t constSegmentValues[11] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0xFF};
	
//store the values off each segment, which is to display
static uint8_t segmentValuesToDisplay[8];
	
void Segment_init()
{
	DDRA = 0xFF;
	DDRC = 0xFF;
	
	//set the value that all segments are turned off
	for (uint8_t i = 0; i < 8; ++i)
	{
		segmentValuesToDisplay[i] = constSegmentValues[SEGMENT_OFF];
	}

	Segment_refreshValues();
	
	//configure timer1 for timing the constant time TIME_REFRESH
	TCCR1B |= ((1<<WGM12)|(1<<CS10)|(1<<CS11)); //CTC mode with OCR1A as top value and prescaler to 64
	TIMSK1 |= (1<<OCIE1A); //activate overflow interrupt on compare match
	OCR1A = (uint16_t)((TIME_REFRESH * F_CPU)/64 - 1); //calculate the value to count
	
	sei();
}
	
void Segment_lightNothing()
{
	SEGMENT_VALUE = 0xFF;
	SEGMENT_SELECT = 0xFF;

}
	
void Segment_lightSegment(uint8_t segmentNum, uint8_t number)
{
	SEGMENT_SELECT = ~(1<<segmentNum);
	SEGMENT_VALUE = constSegmentValues[number];
}

void Segment_setValueForSegment(uint8_t segmentNum, uint8_t number)
{
	if (number < 10)
	{
		if (segmentNum < 8)
		{
			//set value to display on selected segment
			segmentValuesToDisplay[segmentNum] = constSegmentValues[number]; 
		}
	} else 
	{
		//set value to display to turn off the selected segment
		segmentValuesToDisplay[segmentNum] = constSegmentValues[SEGMENT_OFF]; 
	}
}

void Segment_setPointOn(uint8_t segmentNum)
{
	if (segmentNum < 8)
	{
		//change value to display to light the point with 
		//	out changing the value off the selected segment
		segmentValuesToDisplay[segmentNum] &= ~(1<<POINT);
	}
}

void Segment_setPointOff(uint8_t segmentNum)
{
	if (segmentNum < 8)
	{
		//change value to display to turn off the point with
		//	out changing the value off the selected segment
		segmentValuesToDisplay[segmentNum] |= (1<<POINT);
	}
}

void Segment_setSign(uint8_t segmentNum)
{
	if (segmentNum < 8)
	{
		//set Sign ("-") on the selected Segment
		segmentValuesToDisplay[segmentNum] = DISPLAY_SIGN;
	}
}

void Segment_refreshValues()
{
	//go thought all segment and display their stored value
	for (uint8_t i  = 0; i < 8; ++i)
	{
		//set value to bus
		SEGMENT_VALUE = segmentValuesToDisplay[i];
		//activate Segment
		SEGMENT_SELECT = ~(1<<i);
		//wait a specific time then turn the transistor off
		_delay_us(DELAY_SHOW);
		SEGMENT_SELECT = 0xFF;
		//wait a specific time to let the transistor switch
		_delay_us(DELAY_SWITCH);

	}
}

void Segment_lightNextSegment()
{
	static uint8_t	lightedSegment = 0;
	
	//turn off Segments
	SEGMENT_SELECT = 0xFF;

	if (++lightedSegment == 8)
	{
		lightedSegment = 0;
	}
	//write value for segment to light on bus
	SEGMENT_VALUE = segmentValuesToDisplay[lightedSegment];
	//activate segment
	SEGMENT_SELECT = ~(1<<lightedSegment);

	//segment will be active until this functions will be called again
}

//all TIME_REFRESH refresh the display
ISR(TIMER1_COMPA_vect)
{
// 	//refresh all Values
// 	Segment_refreshValues();

	//light the next segment
	Segment_lightNextSegment();
}