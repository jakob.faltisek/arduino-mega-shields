/*
 * print_segment.c
 *
 * Created: 19.04.2016 17:50:49
 *  Author: Jakob
 */ 

#include <avr/interrupt.h>
#include "Segment.h"

void print_uint(uint32_t printValue)
{
	uint32_t	tenHighN = 1;
	
	for(uint8_t n = 0; n < 8; ++n)
	{
		if (tenHighN <= printValue)
		{
			//the digit is no leading zero and has to be displayed
			Segment_setValueForSegment(n, (uint8_t)((printValue/tenHighN)%10));
		} else
		{
			//the value is a leading zero, so to nothing	
			Segment_setValueForSegment(n, 10);	
		}
		tenHighN *= 10;
	}
}

void print_int (int32_t printValue)
{
	if (printValue >= 0)
	{
		//if printValue is positive display with the function for unsigned int
		print_uint((uint32_t)printValue);
	}
	else
	{
		//if printValue is negative display the negated value with the function 
			//for unsigned int and delete 8th digit if there
		print_uint((uint32_t)((-printValue)%10000000));
		//set sign on the last segment
		Segment_setSign(7);	 
	}
}