/*
 * test_printFunctions.c
 *
 * Created: 19.04.2016 17:57:52
 * Author : Jakob
 */ 

#include <avr/io.h>
#include "print_segment.h"

#define		F_CPU		16000000
#include <util/delay.h>


int main(void)
{
	Segment_init();
	
    uint32_t	cnt = 1000;
	uint8_t		cnt8bit = 0;
	int32_t		cntNegative = -10000;
	
	print_int(-1234567);
	
    while (1) 
    {
		--cnt;
		++cnt8bit;
		cntNegative -= 5000;
		
		_delay_ms(10);
		
		//print_int(cnt);
		
		//print_uint(cnt8bit);
		//print_int(cntNegative);
    }
}

