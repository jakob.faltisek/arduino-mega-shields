/*
 * LedMatrix.h
 *
 * Created: 10.11.2015 17:07:40
 *  Author: Jakob
 */ 


#ifndef LEDMATRIX_H_
#define LEDMATRIX_H_

#ifndef		BUTTON1
#define		BUTTON1		PIN6
#define		BUTTON2		PIN4
#endif

const uint8_t arrow_left[8]	= {0x08, 0x0C, 0xFE, 0xFF, 0xFF, 0xFE, 0x0C, 0x08};
const uint8_t arrow_right[8]	= {0x10, 0x30, 0x7F, 0xFF, 0xFF, 0x7F, 0x30, 0x10};
const uint8_t arrow_up[8]	= {0x18, 0x3C, 0x7E, 0xFF, 0x3C, 0x3C, 0x3C, 0x3C};
const uint8_t arrow_down[8]	= {0x3C, 0x3C, 0x3C, 0x3C, 0xFF, 0x7E, 0x3C, 0x18};
	

/*
 * Function: LedMatrix_init
 *		Initializes the DDR for the leds (output) and the buttons (input)
 *
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
void LedMatrix_init();

/*
 * Function: LedMatrix_start
 *		Starts displaying a form on the LED Matrix
 *
 * Parameter: 
 *      in  pFormToDisplay  Adress of the array, in which the form that should
 *                          be displayed is stored.
 *
 * preconditions: 
 *      the driver must be initiallized with LedMatrix_init() 
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
void LedMatrix_start (uint8_t* pFormToDisplay);

/*
 * Function: LedMatrix_stop
 *		The driver starts displaying a form on the LED Matrix
 *
 * preconditions: 
 *      the driver must be initiallized with LedMatrix_init() 
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
void LedMatrix_stop ();

/*
 * Function: LedMatrix_changeForm
 *		The driver should display a different form
 *
 * Parameter: 
 *      in  pFormToDisplay  Adress of the array, in which the form that should
 *                          be displayed is stored.
 *
 * preconditions: 
 *      the driver must be initiallized with LedMatrix_init() 
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
void LedMatrix_changeForm (uint8_t* pFormToDisplay);




#endif /* LEDMATRIX_H_ */