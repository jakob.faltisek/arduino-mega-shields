/*
 * LedMatrix.c
 *
 * Created: 10.11.2015 17:08:26
 *  Author: Jakob
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#ifndef F_CPU
#define F_CPU 16000000
#endif
#include <util/delay.h>

#define		BUTTON1		PIN6
#define		BUTTON2		PIN4

#define		LINES		PORTC
#define		COLUMNS		PORTA

//define length of pause after every line display to control the brightness
#define		PAUSE_US	500

//contains the value for the columns for each line
static volatile uint8_t* pColumnValuesToDisplay;

void LedMatrix_init()
{
	DDRA = 0xFF; //set all columns (anodes) of the leds to output
	DDRC = 0xFF; //set all lines (cathodes) of the leds to output
	
	DDRB &= ~( (1<<BUTTON1) | (1<<BUTTON2) ); //set the two buttons to input
	
	//select pins for PCI
	PCMSK0 |= ((1<<PCINT4) | (1<<PCINT6));	
	
	//timer 2 to multiplex the leds
	//normal mode, pre = 128 -> time for the overflow = 2.042ms
	TCCR2A = 0x00; //normal mode
	TCCR2B = 0x00; //timer 2 stopped
	
	//enable PCI 1 and timer overflow interrupt 2
	PCICR |= (1<<PCIE0);
	TIMSK2 |= (1<<TOIE2);
	sei();
}

void LedMatrix_start (uint8_t* pFormToDisplay)
{
	pColumnValuesToDisplay = pFormToDisplay;
	
	TCCR2B = ((5 << CS20)); //start timer2
}

void LedMatrix_stop ()
{
	TCCR2B = 0x00; //timer 2 stopped
	
	COLUMNS = 0x00;
	LINES = 0xFF;
}

void LedMatrix_changeForm (uint8_t* pFormToDisplay)
{
	pColumnValuesToDisplay = pFormToDisplay;
}

//ever 2ms display the next line
ISR (TIMER2_OVF_vect)
{
	static uint8_t line = 0;
	
	if (++line == 8)
	{
		line = 0;
	}
	
	COLUMNS = 0x00; //turn off the columns
	_delay_us(PAUSE_US);		
	LINES = ~(1<<(7-line)); //light the next line (inverted due to the schematic)
	COLUMNS = *(pColumnValuesToDisplay + line);

}