/*
 * distanceBoard.c
 *
 * Created: 19.11.2016 12:25:55
 *  Author: Jakob
 */ 


#define	F_CPU	16000000
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "distanceBoard.h"

//For testing
#include <communication/uart0.h>

//definitions for the pins of the mux/demux
#define		DDR_TRIG	DDRL
#define		PORT_TRIG	PORTL
#define		TRIG		PL4
#define		DDR_ECHO	DDRL
#define		PIN_ECHO	PINL
#define		ECHO		PL0
#define		DDR_SEL		DDRK
#define		PORT_SEL	PORTK
#define		SEL_A		PK0
#define		SEL_B		PK1


//average time between the trigger signal and the answer on the echo pin
#define		TIME_TRIG_ECHO		460 //us

//defines how many samples get stored for average and min max correction
#define		NUM_STORED_SAMPLES		6

//indicate that the led auto mode is used
static volatile uint8_t		ledAutoMode;	

static volatile uint8_t		cntSelUss;		//indicate the uss which is selected
static volatile uint8_t		echoHappend;	//indicate that an echo happened
static volatile uint16_t	ussSamples[4][NUM_STORED_SAMPLES+2];	//Array to store the uss sample for average and min max correction
static volatile uint8_t		sampleCnt[4];			//indicate which sample has to be written on the ussSamples array

 void dB_init (uint8_t initButtons, uint8_t enableButtonInt, uint8_t initLEDs, uint8_t useAutoModeLed)
{
	//select pins to output
	DDR_SEL |= ((1<<SEL_A)|(1<<SEL_B));
	//trigger pin to output
	DDR_TRIG |= (1<<TRIG);
	//echo pin to input
	DDR_ECHO &= ~(1<<ECHO);
	 	
	if (initButtons)
	{
		DDR_BUTTON &= ((1<<BUTTON_1)|(1<<BUTTON_2));
		
		if (enableButtonInt)
		{
			EICRB |= ((1<<ISC41)|(1<<ISC51));
			EIMSK |= ((1<<INT4)|(1<<INT5));
		}
	}

	if (initLEDs)
	{
		DDR_LED|= ((1<<LED_FR)|(1<<LED_LE)|(1<<LED_RI)|(1<<LED_BA)|(1<<LED_MI));

		if (useAutoModeLed)
		{
			ledAutoMode = 1;
		}
	}

	 	
	//use the input capture unit from the timer 4 to detect a signal change on the echo pin
	TCCR4B |= (/*(1<<ICES4)|*/(1<<ICNC4)); //input capture to trigger on raising edge with noise canceler
	TIMSK4 |= (1<<ICIE4); //enable interrupt on ic event

	//Timer 3 to count every 0.5us, pre = 8
	TCCR3A = 0x00;
	TCCR3B = 0x00; //timer stopped
	TIMSK3 |= (1<<TOIE3);

	sei();

	
}

void selUSS (enum sel_uss uss)
{
	if (uss == USS_FR)
	{
		PORT_SEL &= ~((1<<SEL_A)|(1<<SEL_B));
	}
	else if (uss == USS_RI) {
		PORT_SEL &= ~(1<<SEL_B);
		PORT_SEL |= (1<<SEL_A);
	}
	else if (uss == USS_LE) {
		PORT_SEL &= ~(1<<SEL_A);
		PORT_SEL |= (1<<SEL_B);
	}
	else if (uss == USS_BA){
		PORT_SEL |= ((1<<SEL_A)|(1<<SEL_B));
	}
}

uint16_t dB_readAvgUSSValue (enum sel_uss	 numUss)
{
	uint32_t	average = 0;
	for (uint8_t i = 0; i < NUM_STORED_SAMPLES; i++)
	{
		average += ussSamples[numUss][i];
	}
	average /= NUM_STORED_SAMPLES;

	return average;
}

uint16_t dB_readAvgMinMaxUSSValue (enum sel_uss	 numUss)
{
	uint16_t	min, max;
	uint32_t	average = 0;
	
	min = ussSamples[numUss][0];
	max = ussSamples[numUss][0];
	for (uint8_t i = 0; i < NUM_STORED_SAMPLES; i++)
	{
		average += ussSamples[numUss][i];
		if (ussSamples[numUss][i] > max){
			max = ussSamples[numUss][i];
		}
		else if (ussSamples[numUss][i] < min){
			min = ussSamples[numUss][i];
		}
	}
	average -= (max+min);
	average /= (NUM_STORED_SAMPLES-2);

	return average;
}

uint16_t dB_readUSSValue (enum sel_uss	 numUss)
{
	//return the last sample
	if (sampleCnt[(uint8_t)numUss] != 0)
	{
		return ussSamples[numUss][sampleCnt[numUss]-1];
	} 
	else
	{
		return ussSamples[numUss][NUM_STORED_SAMPLES - 1];
	}
}


//send an 10us echo signal to the ultrasonic sensor to start the measurement
void sendEcho()
{
	cli();
	PORTL |= (1<<TRIG);
	_delay_us(10);
	PORTL &= ~(1<<TRIG);
	sei();
}

//write the value for the uss values in the array to store them
void writeUSSValue (uint16_t value)
{
	ussSamples[cntSelUss][sampleCnt[cntSelUss]] = value;

	//Test
	if (cntSelUss == 0)
	{
// 		uart0_print_uint16(value);
// 		uart0_putc(';');
// 		uart0_print_uint16(dB_readAvgMinMaxUSSValue(0));
// 		uart0_newLine();
	}

	if (++sampleCnt[cntSelUss] == NUM_STORED_SAMPLES)
	{
		sampleCnt[cntSelUss] = 0;
	}

}

//start timer to begin measurment
void dB_startMeasurement ()
{
	TCNT3 = 0;
	TCCR3B |= (1<<CS31); //start timer with pre = 8
}

//stop timer to end measurment
void dB_stopMeasurement ()
{
	TCCR3B = 0x00;
}

//return the value of the timer
uint16_t getValueOfTimer()
{
	return TCNT3;
}

//after every overflow at 32us select the next USS and send the next trigger signal
ISR (TIMER3_OVF_vect)
{
	if (echoHappend)
	{
		echoHappend = 0;
	} 
	else
	{
		writeUSSValue(0); //indicated that no echo has been received

		if (ledAutoMode)
		{
			PORT_LEDS  &= ~(1<<(LED_FR + cntSelUss));
		}
	}

	if (++cntSelUss == 3)
	{
		cntSelUss = 0;
	}
	selUSS(cntSelUss);
	sendEcho();
}


//call on every falling edge on the echo pin
ISR (TIMER4_CAPT_vect)
{

	uint16_t	timeUs;

	echoHappend = 1; 

	//check if the time between trig and echo is greater than the average time
	//otherwise it is a wrong signal, apparently due to the echo pin, which was still high 
	//from the last signal, that did not hit back the sensor
	if ((timeUs = getValueOfTimer()>>1) > TIME_TRIG_ECHO)
	{
		timeUs -= TIME_TRIG_ECHO;

	// 
	// 	uart0_print_uint8(cntUss+1);
	// 	uart0_putc(':');
	// 	uart0_print_uint16(timeUs);
	// 	uart0_newLine();

		writeUSSValue(timeUs);

	
		if (ledAutoMode)
		{
			PORT_LEDS |= (1<<(LED_FR + cntSelUss));
		}

	}
}
