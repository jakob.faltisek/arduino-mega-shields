/*
 * distanceBoard.h
 *
 * Created: 19.11.2016 12:26:30
 *  Author: Jakob
 */ 


#ifndef DISTANCEBOARD_H_
#define DISTANCEBOARD_H_


typedef	enum sel_uss {USS_FR, USS_RI, USS_LE, USS_BA};

#define		DDR_LED		DDRK
#define		PORT_LEDS	PORTK
#define		LED_FR		PK2
#define		LED_RI		PK3
#define		LED_LE		PK4
#define		LED_BA		PK5
#define		LED_MI		PK6

#define		DDR_BUTTON	DDRE
#define		PIN_BUTTON	PINE
#define		BUTTON_1	PE4
#define		BUTTON_2	PE5


/*
 * Function: dB_init
 *		Initializes the driver of the distance board
 *
 * Parameter: 
 *      in  initButtons     0 -> buttons are not activeted  
 *                          1 -> the pins of the 2 buttons are set as inputs
 *      in  enableButtonInt 0 -> don't enable the external interrupt for the buttons
 *                          1 -> enable the external interrupt for the buttons
 *      in initLEDs         0 -> LEDs are not activeted
 *                          1 -> the pins of the 4 LEDs are set as output
 *      in useAutoModeLed   0 -> don't enable AutoMode for the LEDs
 *                          1 -> enable AutoMode for the LEDs, which means the LEDs are
 *                               automatically turned on, when the driver recognizes a US-Sensor
 *                               on the pin header next to the LED
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		26.11.2016
 */
void dB_init (uint8_t initButtons, uint8_t enableButtonInt, uint8_t initLEDs, uint8_t useAutoModeLed);

/*
 * Function: dB_readAvgMinMaxUSSValue
 *		Read the selected US Sensor - the driver takes the stored measurements of the selected sensor, deletes 
 *      the min and the max values to reduce possible errors and takes the average
 *      of the remaining values, to increase the accuracy of the measurement
 *
 * Parameter: 
 *      in  numUss  The enum value of the sensor to read
 *
 * preconditions: 
 *      the driver must be initialized with dB_init() and the measurement must 
 *      been started with dB_startMeasurement
 * 
 * Return value:
 *      Measured time for the selected US Sensor to get the echo of the sound back
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
uint16_t dB_readAvgMinMaxUSSValue (enum sel_uss	 numUss);

/*
 * Function: dB_readAvgUSSValue
 *		Read the selected US Sensor - the driver takes the stored measurements of the selected sensor and takes the average
 *      of the values, to increase the accuracy of the measurement
 *
 * Parameter: 
 *      in  numUss  The enum value of the sensor to read
 *
 * preconditions: 
 *      the driver must be initialized with dB_init() and the measurement must 
 *      been started with dB_startMeasurement
 * 
 * Return value:
 *      Measured time for the selected US Sensor to get the echo of the sound back
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
uint16_t dB_readAvgUSSValue (enum sel_uss	numUss);

/*
 * Function: dB_readUSSValue
 *		Read the selected US Sensor - the driver returns only the most recent measurement
 *
 * Parameter: 
 *      in  numUss  The enum value of the sensor to read
 *
 * preconditions: 
 *      the driver must be initialized with dB_init() and the measurement must 
 *      been started with dB_startMeasurement 
 * 
 * Return value:
 *      Measured time for the selected US Sensor to get the echo of the sound back
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
uint16_t dB_readUSSValue (enum sel_uss	numUss);

/*
 * Function: dB_startMeasurement
 *		Start the measurement of the US Sensores and store the measurements 
 *      to later calculate average values
 *
 * preconditions: 
 *      the driver must be initialized with dB_init() 
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
void dB_startMeasurement();

/*
 * Function: dB_stopMeasurement
 *		Stop the measurement
 *
 * preconditions: 
 *      the driver must be initialized with dB_init() 
 * 
 * Author: Jakob Faltisek
 *
 * Date of last change:
 *		10.11.2015
 */
void dB_stopMeasurement();


#endif /* DISTANCEBOARD_H_ */